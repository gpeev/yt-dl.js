app.module('Models', function (Models, App, Backbone, Marionette, $, _) {
  var fs = require('fs');
  var ytdl = require('ytdl-core');
  var ffmpeg = require('fluent-ffmpeg');

  Models.ListEl = Backbone.Model.extend({
    download: function () {
      var title = this.get('snippet').title;
      var videoId = this.get('snippet').resourceId ? this.get('snippet').resourceId : this.get('id');
      var url = App.Settings.video.getVideoDownloadUrl(videoId);

      fs.exists(App.Settings.downloadDir + title + '.mp3', function (exists) {
        if (!exists) {
          var stream = ytdl(url);
          ffmpeg({ source: stream })
            .noVideo()
            .on('error', function(err) {
              this.trigger('download:error');
            }.bind(this))
            .on('end', function() {
              this.trigger('download:complete');
            }.bind(this))
            .save(App.Settings.downloadDir + title + '.mp3');
        } else {
          this.trigger('download:complete');
        }
      }.bind(this));
    }
  });

  Models.List = Backbone.Collection.extend({
    model: Models.ListEl,

    parse: function (data) {
      this.nextPageToken = data.nextPageToken;
      return data.items;
    }
  });
});
