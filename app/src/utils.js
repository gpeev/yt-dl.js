app.module('Utils', function (Utils, App, Backbone, Marionette, $, _) {
  _.extend(Utils, {
    getQueryVal: function(url, key) {
      var parser = document.createElement('a');
      parser.href = url;

      var query = parser.search.substring(1).split('&');
      query = query.map(function (pair) {
        var splistPair = pair.split('=');
        return {
          key: splistPair[0],
          value: splistPair[1]
        };
      });

      var pair = _.findWhere(query, {key: key});
      return pair ? pair.value : null;
    }
  });
});
