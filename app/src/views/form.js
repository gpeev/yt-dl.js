app.module('Views', function(Views, App, Backbone, Marionette, $, _){
  Views.Form = Marionette.ItemView.extend({
    className: 'input-group',

    template: JST.form,

    ui: {
      url: '#video-url'
    },

    events: {
      'click a': 'onSubmit'
    },

    initialize: function () {
    },

    onSubmit: function (e) {
      e.preventDefault();

      var url = this.ui.url.val();
      var type = this.$(e.target).attr('data-type');

      if (type == 'playlist') {
        var playlistId = App.Utils.getQueryVal(url, 'list');
        App.vent.trigger('list:fetch:playlist', playlistId);
      } else {
        var videoId = App.Utils.getQueryVal(url, 'v');
        App.vent.trigger('list:fetch:video', videoId);
      }
    }
  });
});
