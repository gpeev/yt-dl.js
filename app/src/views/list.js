app.module("Views", function (Views, App, Backbone, Marionette, $, _) {
  Views.ListEl = Marionette.ItemView.extend({
    template: JST.listEl,
    tagName: 'tr',

    ui: {
      download: 'input',
      spinner: '.glyphicon-spin',
      success: '.gluphicon-ok',
      error: '.glyphicon-remove'
    },

    modelEvents: {
      'download:complete': 'onDownloadComplete',
      'download:error': 'onDownloadError'
    },

    onDownloadRequest: function () {
      this.ui.spinner.removeClass('hidden');
    },

    onDownloadComplete: function () {
      this.ui.spinner.addClass('hidden');
      this.ui.success.removeClass('hidden');
    },

    onDownloadError: function () {
      this.ui.spinner.addClass('hidden');
      this.ui.error.removeClass('hidden');
    },

    download: function (force) {
      if (force || this.ui.download.is(':checked')) {
        this.onDownloadRequest();
        this.model.download();
      }
    }
  });

  Views.List = Marionette.CompositeView.extend({
    template: JST.list,

    childView: Views.ListEl,
    childViewContainer: 'tbody',

    ui: {
      checkAll: '#check-all',
      download: '.btn-download'
    },

    events: {
      'click @ui.checkAll': 'onClickCheckAll',
      'click @ui.download': 'onDownloadClick'
    },

    initialize: function () {
      this.listenTo(App.vent, 'list:download:all', this.downloadAll, this);
    },

    downloadAll: function () {
      this.children.call('download', true);
    },

    downloadSelected: function () {
      this.children.call('download');
    },

    onClickCheckAll: function () {
      var value = this.allChecked() ? false : true;
      this.children.each(function (child) {
        child.ui.download.prop('checked', value);
      });
    },

    allChecked: function () {
      return this.children.all(function (child) {
        return child.ui.download.is(':checked');
      });
    },

    fetchPlaylist: function (playlistId) {
      var url = App.Settings.playlist.getPlaylistUrl(playlistId);
      this.collection.reset();
      this.collection.fetch({
        url: url,
        reset: true,
        parse: true
      });
      this.render();
    },

    fetchVideo: function (videoId) {
      var url = App.Settings.video.getVideoFetchUrl(videoId);
      this.collection.reset();
      this.collection.fetch({
        url: url,
        reset: true,
        parse: true
      });
      this.render();
    },

    onDownloadClick: function () {
      this.downloadSelected();
    }
  });
});
