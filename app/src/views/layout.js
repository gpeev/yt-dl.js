app.module('Views', function(Views, App, Backbone, Marionette, $, _){
  Views.Layout = Backbone.Marionette.LayoutView.extend({
    template: JST.layout,

    regions: {
      form: '#form',
      list: '#list'
    },

    initialize: function () {
      this.formView = new Views.Form({
        model: new App.Models.Form()
      });
      this.listView = new Views.List({
        collection: new App.Models.List()
      });
      this.listView.listenTo(App.vent, 'list:fetch:playlist', this.onPlaylistRequest);
      this.listView.listenTo(App.vent, 'list:fetch:video', this.onVideoRequest);
    },

    onShow: function () {
      this.form.show(this.formView);
      this.list.show(this.listView);
    },

    onPlaylistRequest: function (playlistId) {
      this.fetchPlaylist(playlistId);
    },

    onVideoRequest: function (videoId) {
      this.fetchVideo(videoId);
    }
  });
});
