app.module('Settings', function (Settings, App, Backbone, Marionette, $, _) {
  _.extend(Settings, {
    playlist: {
      baseUrl: 'https://www.googleapis.com/youtube/v3/playlistItems?',
      query: [
        'part=snippet',
        'maxResults=50',
        'key=AIzaSyD_QUysFf6GVSni0hLPCkp9f51IJ2Y-5QM'
      ],

      getPlaylistUrl: function (playlistId) {
        return this.baseUrl + this.query.join('&') + '&playlistId=' + playlistId;
      }
    },
    video: {
      baseUrl: 'https://www.youtube.com/watch?v=',
      baseApiUrl: 'https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyD_QUysFf6GVSni0hLPCkp9f51IJ2Y-5QM&id=',

      getVideoDownloadUrl: function (videoId) {
        return this.baseUrl + videoId;
      },

      getVideoFetchUrl: function(videoId) {
        return this.baseApiUrl + videoId;
      }
    },
    downloadDir: '/home/peio/music/'
  });
});
