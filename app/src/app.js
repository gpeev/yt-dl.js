var YtDl = Backbone.Marionette.Application.extend({
  initialize: function () {

  }
});

var app = new YtDl();

app.addRegions({
  main: '#content'
});

app.on('start', function() {
  var layout = new app.Views.Layout();
  app.main.show(layout);
});
