module.exports = function(grunt) {

  grunt.initConfig({
    clean: ['app/libs/*.js', 'app/js/*.js', 'app/css/*.css'],
    copy: {
      libs: {
        expand: true,
        src: [
          'bower_components/underscore/underscore.js',
          'bower_components/jquery/dist/jquery.js',
          'bower_components/bootstrap/dist/js/bootstrap.js',
          'bower_components/handlebars/handlebars.js',
          'bower_components/backbone/backbone.js',
          'bower_components/marionette/lib/backbone.marionette.js'
        ],
        dest: 'app/libs/',
        flatten: true
      },
      css: {
        expand: true,
        src: [
          'bower_components/bootstrap/dist/css/bootstrap.css',
          'bower_components/bootstrap/dist/css/bootstrap.css.map'
        ],
        dest: 'app/css/',
        flatten: true
      },
      fonts: {
        expand: true,
        src: [
          'bower_components/bootstrap/dist/fonts/*'
        ],
        dest: 'app/fonts',
        flatten: true
      }
    },
    concat: {
      libs: {
        src: [
          'app/libs/underscore.js',
          'app/libs/jquery.js',
          'app/libs/bootstrap.js',
          'app/libs/handlebars.js',
          'app/libs/backbone.js',
          'app/libs/backbone.babysitter.js',
          'app/libs/backbone.wreqr.js',
          'app/libs/backbone.marionette.js'
          ],
        dest: 'app/js/libs.js'
      },
      app: {
        src: 'app/src/**/*.js',
        dest: 'app/js/app.js'
      },
      css: {
        src: 'app/css/*.css',
        dest: 'app/css/style.css'
      }
    },
    handlebars: {
      compile: {
        options: {
          namespace: "JST",
          processName: function (filePath) {
            var pieces = filePath.split("/");
            return pieces[pieces.length - 1].split('.')[0];
          }
        },
        files: {
          'app/js/templates.js' : [ 'app/templates/*.hbs']
        }
      }
    },
    fileblocks: {
      options: {
        rebuild: true
      },
      dev: {
        src: 'app/index.html',
        blocks: {
          'styles': {
            cwd:'app',
            src: 'css/style.css'
          },
          'app': {
            cwd:'app',
            src: [
              'js/libs.js',
              'js/templates.js',
              'js/app.js'
            ]
          }
        }
      },
      dist: {
        src: 'app/index.html',
        blocks: {
          'css': {cwd:'app', src: 'css/style.min.css'},
          'js': {cwd:'app', src: 'js/*.min.js'}
        }
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'app/src/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'app/js/libs.js': ['app/js/libs.min.js'],
          'app/js/app.js': ['app/js/app.min.js'],
          'app/js/templates.js': ['app/js/templates.min.js']
        }
      }
    },
    watch: {
      files: ['<%= jshint.files %>', 'app/templates/*.hbs'],
      tasks: ['jshint','concat:app','handlebars']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-handlebars');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-file-blocks');

  grunt.registerTask('default', []);
  grunt.registerTask('dev', ['clean', 'copy', 'jshint', 'concat', 'handlebars', 'fileblocks:dev', 'watch']);
};
